This repository contains OCaml package metadata bundled by [Nicolas
Berthier](http://nberth.space).  It is mainly intended for the
distribution of research prototype implementations.

## Purpose

This repository is a more secured version of the soon-to-be-deprecated
one hosted at `http://nberth.space/opam-repo`.

## License

All the metadata contained in this repository are licensed under the
[CC0 1.0 Universal](http://creativecommons.org/publicdomain/zero/1.0/)
license.

Moreover, as the collection of the metadata in this repository is
technically a "Database" -- which is subject to a "sui generis" right
in Europe -- we would like to stress that even the *collection* of
the metadata contained in opam-repository is licensed under CC0 and
thus the simple act of cloning opam-repository is perfectly legal.
